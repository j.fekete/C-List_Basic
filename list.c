#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int value;
    struct node *next;
    struct node *prev;
} node_t;

node_t *create_new_node(int value){
    node_t *result = malloc(sizeof(node_t));
    result->next = NULL;
    result->value = value;
    result->prev = NULL;
    return result;
}

node_t *insert_at_head(node_t *head,node_t *node_to_head){
     node_to_head->next = head;
     if (head!=NULL)
     {
         head->prev = node_to_head;
     } 
     head = node_to_head;
     return node_to_head;
}

node_t *find_node(node_t *head,int value){
    node_t *tmp = head;
    while (tmp != NULL)
    {
        if (tmp->value == value) return tmp;
        tmp = tmp->next; 
    }
    return NULL;
}

void insert_after_node(node_t *Node_to_insert_after,node_t *newnode){
    newnode->next = Node_to_insert_after->next;
    if (newnode->next != NULL)
    {
        newnode->next->prev = newnode;
    }
    newnode->prev = Node_to_insert_after;
    Node_to_insert_after->next = newnode;
}

void insert_before_node(node_t *Node_to_insert_before,node_t *newnode){
    newnode->prev = Node_to_insert_before->prev;
    if (newnode->prev!=NULL)
    {
       newnode->prev->next=newnode;
    }
    
    newnode->next= Node_to_insert_before;
    Node_to_insert_before->prev = newnode;
}

void remove_node(node_t *head,node_t *node_to_remove){
    
    if (head == node_to_remove)
    {
        head = node_to_remove->next;
        if (head != NULL)
        {
         head->prev = NULL;
        }
    }else
    {
             node_to_remove->prev->next = node_to_remove->next;
             if (node_to_remove!=NULL)
             {
                 node_to_remove->next->prev = node_to_remove->prev;
             }
    } 
    node_to_remove->prev=NULL;
    node_to_remove->next=NULL;
    return;   
}

void printlist(node_t *head){
    node_t *tmp = head;
    while (tmp != NULL)
    {
        printf("<<--%d ",tmp->value);
        tmp = tmp->next;
    }
    printf("\n");
}

int main()
{
    node_t *head = NULL;
    node_t *tmp;

    for (int i = 50; i > 0; i--)
    {
        tmp = create_new_node(i);
        head=insert_at_head(head,tmp);
    }
    tmp = find_node(head,5);
    printf("Node %d found at adress %p\n",tmp->value,tmp);
    printlist(head);
    printf("\n\n");
    insert_after_node(tmp,create_new_node(52));
    printlist(head);
    printf("\n\n");
    remove_node(head,find_node(head,10));
    printlist(head);
    printf("\n\n");
    head=insert_at_head(head,create_new_node(0));
    printlist(head);
    return 0;
}

